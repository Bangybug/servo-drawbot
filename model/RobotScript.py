# Python Blender script used for robot movement prototyping
# The result of the script is the keyframe animation sequence created by anim* methods in CAnimator class
# Physics simulation cannot be easily implemented in Blender, because they have removed Blender game engine in 2.8
# @author Dmitry Pryadkin, drpadawan@ya.ru, https://vk.com/bug_dog

import bpy
import colorsys
from math import sqrt, pi, sin, cos, ceil, atan, acos, sqrt, pow, radians, degrees
from random import TWOPI

#
# Animation variables.
#

SLOWDOWN = 100
FPS = 30 * SLOWDOWN

#
# Robot properties
#

SG90_SERVO_SPEED = radians(60) / 0.15  

CRANK_SMALL = 0
CRANK_LONG = 1
LINK = 2
PEN_ARM = 3
LEVER_Z = 4

# CModel class defines 3d model of the robot, model parts are linked in Blender using parent-child relationship
class CModel:
    smallCrank = bpy.data.objects["Crank-small-bottom"]
    longCrank = bpy.data.objects["Crank-long-top"]
    link = bpy.data.objects["Link-loose"]
    penArm = bpy.data.objects["Pen-arm"]
    base = bpy.data.objects["Frame"]
    debugA = bpy.data.objects["PointA"]
    debugB = bpy.data.objects["PointB"]
    debugC = bpy.data.objects["PointC"]

    def __init__(self):
        self.models = [self.smallCrank, self.longCrank, self.link, self.penArm, self.base]
        
    def setPartAngle(self, partIndex, angle): 
        self.models[partIndex].rotation_euler = (angle, 0.0, 0.0) if partIndex == LEVER_Z else (0.0, 0.0, angle)
        self.models[partIndex].keyframe_insert(data_path='rotation_euler')


# CDimensions class defines basic dimensions of the robot in millimeters
# dimensions can be measured either in Blender or on real 3d-printed objects, lengths measured between shafts (joints)
# The original design has two lengths: 30 and 50
class CDimensions: 
    penArm = 50
    penArmAttach = 30
    link = 50
    crankHigh = 50
    crankLow = 30

# CLimits class defines limits, in degrees for the seromotors.  This does not help avoiding self-collisions.
class CLimits:
    servoBottom = [ radians(0), radians(90) ]
    servoTop = [ radians(-45), radians(-145) ]
    servoZ = [ radians(0), radians(90) ]
    

# CMovementIK takes desired tip position (x,y,z) in millimeters as input and returns angles per each servo.
# Zero position is determined by servo motor shaft (x,y,z=0)
class CMovementIK:
    def __init__(self, dimensions):
        self.dimensions = dimensions
        self.abc = [ [0,0], [0,0], [0,0] ]
        self.err = False

    def calculateAnglesForPos(self, x,y,z):
        self.err = True
        d = sqrt( x*x + y*y)
        if d > self.dimensions.crankHigh + self.dimensions.penArm:
            print('err: outer circles separated')
        elif d < abs(self.dimensions.crankHigh - self.dimensions.penArm):
            print('err: outer circles contained')
        else:
            a = (pow(self.dimensions.crankHigh,2) - pow(self.dimensions.penArm,2) + pow(d,2)) / (2*d)
            h = sqrt( pow(self.dimensions.crankHigh,2) - a*a )
            Ax = (a*x - h*y) / d
            Ay = (a*y + h*x) / d
            self.abc[0] = [Ax,Ay]

            r = (self.dimensions.penArmAttach + self.dimensions.penArm) / self.dimensions.penArm
            Bx = (Ax - x) * r + x
            By = (Ay - y) * r + y
            self.abc[1] = [Bx,By]

            d = sqrt( Bx*Bx + By*By )
            if d > self.dimensions.link + self.dimensions.crankLow:
                print('err: inner circles separated')
            elif d < abs(self.dimensions.link - self.dimensions.crankLow):
                print('err: inner circles contained')
            else:
                self.err = False
                a = (pow(self.dimensions.crankLow,2) - pow(self.dimensions.link,2) + pow(d,2)) / (2*d)
                h = sqrt( pow(self.dimensions.crankLow,2) - a*a )
                Cx = (a*Bx - h*By) / d
                Cy = (a*By + h*Bx) / d
                self.abc[2] = [Cx,Cy]
                
                angleTopLever = atan(Ay/Ax)
                angleBottomLever = atan(Cy/Cx)
                penArmAngle = -pi + (angleBottomLever - angleTopLever)
                linkAngle = -(angleBottomLever - angleTopLever)
                return [angleBottomLever, angleTopLever, linkAngle, penArmAngle]


# CAnimator class provides anim* method group for making animations
class CAnimator:
    def __init__(self, dimensions, model, ik):
        self.dimensions = dimensions
        self.model = model
        self.ik = ik
        self.angles = [0,0,0,0]


    def eraseAllKeyframes(self):
        for ob in bpy.context.scene.objects:
            if ob.animation_data != None:
                ob.animation_data_clear()


    def setKeyframeFromScene(self):
        bpy.context.scene.frame_set(1)
        for modelIndex in range(len(self.model.models)):
            model = self.model.models[modelIndex]
            if modelIndex < LEVER_Z:
                self.angles[modelIndex] = model.rotation_euler[ 2 ]
            model.keyframe_insert(data_path='rotation_euler')
            model.keyframe_insert(data_path='location')
        self.model.debugA.keyframe_insert(data_path='location')
        self.model.debugB.keyframe_insert(data_path='location')
        self.model.debugC.keyframe_insert(data_path='location')


    def animPart(self, partIndex, angle, currentFrame, keyFrames=0, frameCount=0):
        # instantly move
        if keyFrames == 0:
            bpy.context.scene.frame_set(currentFrame)
            self.angles[partIndex] = angle
            self.model.setPartAngle(partIndex, angle)
            return currentFrame+1
        # move by keyFrames from currentFrame
        else: 
            oldAngle = self.angles[partIndex]
            self.angles[partIndex] = angle
            delta = angle - oldAngle
            # print( "DELTA={0} oldangle={1} newangle={2}".format(delta, oldAngle, angle) )
            if frameCount == 0:
                frameCount = ceil(abs(delta) / SG90_SERVO_SPEED * FPS)
            endFrame = currentFrame + frameCount
            invKeyframeCount = 1.0 / keyFrames
            framesPerKey = frameCount * invKeyframeCount
            for f in range(0, keyFrames):
                currentFrame += framesPerKey
                animProgress = (f+1) * invKeyframeCount
                bpy.context.scene.frame_set(currentFrame)
                self.model.setPartAngle(partIndex, oldAngle+delta*animProgress)
            return endFrame


    def animTo(self, pos, currentFrame, keyFrames=0):
        angles = self.ik.calculateAnglesForPos(pos[0], pos[1], 0)
        lastFrame = currentFrame
        if not ik.err:
            lastFrame = self.animPart(CRANK_SMALL, angles[CRANK_SMALL], currentFrame, keyFrames)
            lastFrame = max(lastFrame, self.animPart(CRANK_LONG, angles[CRANK_LONG], currentFrame, keyFrames))
            armsMovementFrames = lastFrame-currentFrame
            lastFrame = max(lastFrame, self.animPart(PEN_ARM, angles[PEN_ARM], currentFrame, keyFrames, armsMovementFrames))
            lastFrame = max(lastFrame, self.animPart(LINK, angles[LINK], currentFrame, keyFrames, armsMovementFrames))
        else:
            print('Calculation has returned an error')

        self.animDebug(currentFrame)
        return lastFrame


    def animDebug(self, currentFrame):
        bpy.context.scene.frame_set(currentFrame)
        print( "A({0},{1}) B({2},{3}) C({4},{5})".format(self.ik.abc[0][0], self.ik.abc[0][1], self.ik.abc[1][0], self.ik.abc[1][1], self.ik.abc[2][0],self.ik.abc[2][1]) )

        self.model.debugA.location.x = self.ik.abc[0][0]/1000
        self.model.debugA.location.y = self.ik.abc[0][1]/1000
        self.model.debugA.keyframe_insert(data_path='location')

        self.model.debugB.location.x = self.ik.abc[1][0]/1000
        self.model.debugB.location.y = self.ik.abc[1][1]/1000
        self.model.debugB.keyframe_insert(data_path='location')

        self.model.debugC.location.x = self.ik.abc[2][0]/1000
        self.model.debugC.location.y = self.ik.abc[2][1]/1000
        self.model.debugC.keyframe_insert(data_path='location')


    def animSilence(self, currentFrame, seconds):
        lastFrame = currentFrame+self.framesPerSecondRealtime(seconds);
        bpy.context.scene.frame_set(lastFrame)
        self.model.setPartAngle(CRANK_LONG, self.angles[CRANK_LONG])
        self.model.setPartAngle(CRANK_SMALL, self.angles[CRANK_SMALL])
        self.model.setPartAngle(PEN_ARM, self.angles[PEN_ARM])
        self.model.setPartAngle(LINK, self.angles[LINK])
        self.model.debugA.keyframe_insert(data_path='location')
        self.model.debugB.keyframe_insert(data_path='location')
        self.model.debugC.keyframe_insert(data_path='location')
        return lastFrame

    def framesPerSecondRealtime(self,seconds):
        return (FPS / SLOWDOWN) * seconds


print("\nScript started")

dimensions = CDimensions()
model = CModel()
limits = CLimits()
ik = CMovementIK(dimensions)
animator = CAnimator(dimensions, model, ik)

animator.eraseAllKeyframes()
bpy.context.scene.frame_set(0)
animator.setKeyframeFromScene()

lastFrame = 1
lastFrame = animator.animTo([30,-20,0], lastFrame, 1)
lastFrame = animator.animSilence(lastFrame, 1)
lastFrame = animator.animTo([40,-20,0], lastFrame, 1)
lastFrame = animator.animSilence(lastFrame, 1)
lastFrame = animator.animTo([30,-10,0], lastFrame, 1)
lastFrame = animator.animSilence(lastFrame, 1)

print(lastFrame)

bpy.context.scene.frame_set(0)
bpy.context.scene.frame_end = lastFrame
