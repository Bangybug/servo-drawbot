### A clone of the LineUS Clone

LineUs is a little drawing robot from kickstarter - https://www.youtube.com/watch?v=8e5LcYbRWkU
It implements SCARA kinematics using inexpensive servo motors.

There is a clone https://www.youtube.com/watch?v=7A0BxJUWkjo - you may look through comments and find its sources.

This project is a clone of the LineUS clone mentioned above :) It is not yet finished.

# 3d printing

I wanted to use Blender here, not CAD. I converted the original models to Blender from Autodesk.  I made some adjustments to fit my sg90 cheap servos.  There are deviations in servo dimensions, so you've got to be careful if you want to print it. 

I took bearings from old 5.25 floppy drives, I had to change the original clone design a bit. The original design had even smaller bearings.

I 3dprinted the mechanism part by part: body, arms, base. You may need to make adjustments to the arms, especially where they attach to the servo gears. It's better to keep arms as close together as possible.

# Debugging kinematics in Blender

Blender is also used for its Python scripting.  I always want to debug kinematics in software - I can do that anywhere on my laptop. This part is pretty much working, go check model/animation.blend

The script is in model/RobotScript.py - it is loaded in animation.blend.  It calculates the inverse kinematics for a given x,y,z point, the animation sequence is created by this script at the end:

lastFrame = 1
lastFrame = animator.animTo([30,-20,0], lastFrame, 1)
lastFrame = animator.animSilence(lastFrame, 1)
lastFrame = animator.animTo([40,-20,0], lastFrame, 1)
lastFrame = animator.animSilence(lastFrame, 1)
lastFrame = animator.animTo([30,-10,0], lastFrame, 1)
lastFrame = animator.animSilence(lastFrame, 1)

You can play it in blender. 

# Firmware

The firmware is written for NodeMCU esp866 in Arduino.  It creates a Http server that listens for commands.  It only moves servos so it is not calculating kinematics. It can stuck the servos if used inappropriately.

# Frontend

The original clone author invested his time into porting GRBL from Arduino to C for PSoC Cypress microcontrollers. GRBL itself is a little firmware for CNC routers and it can be used together with controller software such as Easel. 

My idea for frontend was different. I did not have a Cypress microcontroller, nor I wanted GRBL. I only wanted easy wi-fi, javascript controlling software and html interface. 

So Frontend is made in html/javascript. I wanted it to accept an image, find its contours and send it to device. So far only contour detection is there.
