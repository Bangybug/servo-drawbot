(function() {
  var image;
  var contourFinder;
  var startTime = 0;
  var maxResolution = 400;
  var maxImageWidth = 180;
  var maxImageHeight = 600;

  var resultWidth;
  var resultHeight;

  var imageWidth, imageHeight;

  var filters,
      canny,
      canvas;

  var DIRECTIONS = {
    N: 0,
    NE: 1,
    E: 2,
    SE: 3,
    S: 4,
    SW: 5,
    W: 6,
    NW: 7,
    SAME: 8
  };

  image = document.getElementById('image');

  var dropAreaElement = document.querySelector('.main');
  var imageProvider = new ImageProvider({
    element: dropAreaElement,
    onImageRead: function(image) {
      dropAreaElement.classList.add('dropped');
      imageWidth = image.width;

      if (image.width > image.height) {
        resultWidth = Math.min(image.width, maxResolution);
        resultHeight = parseInt(resultWidth * image.height / image.width, 10);
        imageWidth = Math.min(image.width, maxImageWidth);
        imageHeight = parseInt(imageWidth * image.height / image.width, 10);
      } else {
        resultHeight = Math.min(image.height, maxResolution);
        resultWidth = parseInt(resultHeight * image.width / image.height, 10);
        imageHeight = Math.min(image.height, maxImageHeight);
        imageWidth = parseInt(imageHeight * image.width / image.height, 10);
      }

      contourFinder = new ContourFinder();
      canvas = new Canvas('canvas', resultWidth, resultHeight);
      canny = new Canny(canvas);
      filters = new Filters(canvas);

      image.classList.add('loaded');
      image.style.opacity = 0;

      // delete previous images
      var prev = document.querySelector('.container img.loaded');
      if (prev) {
        prev.parentNode.removeChild(prev);
      }

      var polylines = document.querySelectorAll('#svg2 polyline');
      if (polylines.length) {
        for (var i = 0; i < polylines.length; i++) {
          polylines[i].parentNode.removeChild(polylines[i]);
        }
      }

      image.setAttribute('style', 'width:' + imageWidth + 'px');
      image.setAttribute('style', 'height:' + imageHeight + 'px');
      document.querySelector('.container').appendChild(image);

      canvas.loadImg(image.src, 0, 0, resultWidth, resultHeight).then(process);
    }
  });

  imageProvider.init();


  function process() {
    startTime = Date.now();

    canvas.setImgData(filters.grayscale());
    canvas.setImgData(filters.gaussianBlur(5, 1));

    canvas.setImgData(canny.gradient('sobel'));
    canvas.setImgData(canny.nonMaximumSuppress());
    canvas.setImgData(canny.hysteresis());

    contourFinder.init(canvas.getCanvas());
    contourFinder.findContours();

    // console.log('contourFinder.allContours.length): ' + contourFinder.allContours.length);
    // var secs = (Date.now() - startTime) / 1000;
    // console.log('Finding contours took ' + secs + 's');

    drawContours();
  }

  function findOutDirection(point1, point2) {
    if (point2.x > point1.x) {
      if (point2.y > point1.y) {
        return DIRECTIONS.NE;
      } else if (point2.y < point1.y) {
        return DIRECTIONS.SE;
      } else {
        return DIRECTIONS.E;
      }
    } else if (point2.x < point1.x) {
      if (point2.y > point1.y) {
        return DIRECTIONS.NW;
      } else if (point2.y < point1.y) {
        return DIRECTIONS.SW;
      } else {
        return DIRECTIONS.W;
      }
    } else {
      if (point2.y > point1.y) {
        return DIRECTIONS.N;
      } else if (point2.y < point1.y) {
        return DIRECTIONS.S;
      } else {
        return DIRECTIONS.SAME;
      }
    }
  }

  function drawContours() {
    for (var i = 0; i < contourFinder.allContours.length; i++) {
      //console.log('contour #' + i + ' length: ' + contourFinder.allContours[i].length);
      drawContour(i);
    }
    animate();
  }

  function drawContour(index) {
    var points = contourFinder.allContours[index];

    var optimizedPoints = [],
        direction = null;

    // TODO optimize based on distance
    // points.reduce(function(accumulator, currentValue, currentIndex, array) {
    //   if (optimizedPoints.length === 0) {
    //     optimizedPoints.push(currentValue);
    //     return null;
    //   } else {
    //     var direction = findOutDirection(currentValue, array[currentIndex - 1]);
    //     if (direction === DIRECTIONS.SAME) {
    //       return accumulator;
    //     }
    //     if (direction !== accumulator) {
    //       optimizedPoints.push(currentValue);
    //     } else {
    //       optimizedPoints[optimizedPoints.length -1] = currentValue;
    //     }
    //     return direction;
    //   }
    // }, null);

    optimizedPoints = points;

    var pointsString = optimizedPoints.map(function(point) {
      return point.x + ',' + point.y;
    }).join(' ');

    var polyline = document.createElementNS('http://www.w3.org/2000/svg','polyline');
    polyline.setAttributeNS(null, 'points', pointsString.trim());

    var svg = document.querySelector('#svg2');
    svg.appendChild(polyline);
    svg.setAttribute('viewBox', '0 0 ' + resultWidth + ' ' + resultHeight);
    svg.setAttribute('style', 'width:' + imageWidth + 'px');
    svg.setAttribute('style', 'height:' + imageHeight + 'px');
  }

  function animate() {
    var polylines = document.querySelectorAll('#svg2 polyline');
    [].forEach.call(polylines, function(polyline, index) {
      var length = contourFinder.allContours[index].length;
      // Clear any previous transition
      polyline.style.transition = polyline.style.WebkitTransition =
        'none';

      // Set up the starting positions
      polyline.style.strokeDasharray = length + ' ' + length;
      polyline.style.strokeDashoffset = length;
      // Trigger a layout so styles are calculated & the browser
      // picks up the starting position before animating
      polyline.getBoundingClientRect();
      // Define our transition
      polyline.style.transition = polyline.style.WebkitTransition =
        'stroke-dashoffset 2s linear';
      // Go!
      polyline.style.strokeDashoffset = '0';
    });

    setTimeout(function() {
      document.querySelector('.container img.loaded').style.opacity = 0.2;
      document.querySelector('.container svg').style.opacity = 0.7;
    }, 2500);
  }
})();
