#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <Servo.h>

ESP8266WebServer server(80);

ESP8266WiFiMulti wifiMulti;


String apName = "myhomeap";
String apPassword = "422866565";
// String apName = "Bangybug";
// String apPassword = "azrud98m";

// Domain name for the mDNS responder, the service is available as http://drawbot.local  (you don't have to find the IP address)
const char* mdnsName = "drawbot"; 

bool wifiShouldRestart = true;
bool wifiIsRestarting = false;
unsigned int scheduleRestart = 0;

// maps to D0 on NodeMcu board
// Should travel from -45 to 135 degrees, if looking from the top. 
// -45 degrees is at 2450 us, 135 degrees is at 530 us
#define PIN_SERVO_UPPER 16

// maps to D1
// Should travel from 45 to 225, if looking from the top.
// 45 degrees is at 500 us, 225 is at 2450 us.
#define PIN_SERVO_LOWER 5

// maps to D2
// TODO in current setup, the lever servo at zero angle gets stuck, therefore it must not be set at that angle
// the safe pulse is 2300 (pen down), pen up is at 1400, need to reverse control to not avoid servo being stuck at zero angle
#define PIN_SERVO_LEVER 4

Servo servoUpper, servoLower, servoLever;

void setup() 
{
  Serial.begin(115200);
  delay(10);
  Serial.println("\r\n");

  WiFi.onStationModeDisconnected(&restartWiFi);
  wifiMulti.addAP(apName.c_str(), apPassword.c_str());
  Serial.println("AP registered");

  startMDNS();

  startHttpServer();

  // FYI do not attach at the beginning, 
  //servoUpper.attach(PIN_SERVO_UPPER);
  //servoLower.attach(PIN_SERVO_LOWER);
  //servoLever.attach(PIN_SERVO_LEVER);
}

void handleWifiRestart() 
{
  if (!wifiIsRestarting)
  {
    wifiIsRestarting = true;
  }

  if (wifiMulti.run() == WL_CONNECTED)
  {
    Serial.print("Connected to ");
    Serial.println(WiFi.SSID());             // Tell us what network we're connected to
    Serial.print("IP address:\t");
    Serial.print(WiFi.localIP());            // Send the IP address of the ESP8266 to the computer

    wifiShouldRestart = false;
  }

  if (wifiShouldRestart)
    delay(250);
  else
    wifiIsRestarting = false;
}

void restartWiFi(const WiFiEventStationModeDisconnected& e)
{
  wifiShouldRestart = true;
  wifiIsRestarting = false;
}

void startMDNS() 
{ 
  MDNS.begin(mdnsName);
  Serial.print("mDNS responder started: http://");
  Serial.print(mdnsName);
  Serial.println(".local");
}


void enableCors() {
  server.sendHeader("Access-Control-Allow-Methods", "POST,GET,OPTIONS");
  server.sendHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  server.sendHeader("Access-Control-Allow-Origin", "*");
}


void startHttpServer() 
{
  server.begin();

  server.on("/", HTTP_GET, []() {
    enableCors();
    server.send(200, "text/plain", "get ok" );
  });
  
  server.on("/", HTTP_OPTIONS, []() {
    server.sendHeader("Access-Control-Max-Age", "10000");
    enableCors();
    server.send(200, "text/plain", "" );
  });

  server.on("/", HTTP_POST, []() {
    if (server.hasArg("commands")) 
      processCommands(server.arg("commands"));

    enableCors();
    
    server.send(200, "text/plain", "ok" );
   });
    
   Serial.println("HTTP server started.");
}


#define STATE_READNUMBER 1
#define STATE_READING_NUMBER 2
#define STATE_READ_NEXT 0

void processCommands(const String &commands) {
  const int len = commands.length();
  int state = 0;
  int istart, iend;
  int tokens[] = {0,0,0};
  int numread = 0;
  
  for (int i=0; i<=len; ++i) {
    const char c = i == len ? 'G' : commands.charAt(i);
    if (c == '=' || c == ' ')
      continue;

    if (c == 'G') {
      if (numread == 2 && state == STATE_READING_NUMBER) {
        tokens[numread++] = atoi( commands.substring(istart, iend+1).c_str() );
        processCommand(tokens);
      }

      state = STATE_READNUMBER;
      numread = 0;
    } else {
      if (state > STATE_READ_NEXT) {
        if (c >= '0' && c <= '9') {
          if (state == STATE_READNUMBER) {
            state = STATE_READING_NUMBER;
            istart = i;
            iend = i;
          } else if (state == STATE_READING_NUMBER) {
            ++iend;
          }
        } else if (c == ',') {
          if (state == STATE_READING_NUMBER) {
            if (numread < 2) {
              tokens[numread++] = atoi( commands.substring(istart, iend+1).c_str() );
              state = STATE_READNUMBER;
            } else {
              state = STATE_READ_NEXT;
            }
          }
        }
        else {
          state = STATE_READ_NEXT;
        }
      }
    }
  }
}


void processCommand(int *tokens) {
  Serial.print("Command received ");
  Serial.print(tokens[0]);
  Serial.print(tokens[1]);
  Serial.println(tokens[2]);
  
  if (tokens[0] == 0) {
    servoUpper.detach();
  } else {
    servoUpper.attach(PIN_SERVO_UPPER);
    servoUpper.writeMicroseconds(tokens[0]);
  }

  if (tokens[1] == 0) {
    servoLower.detach();
  } else {
    servoLower.attach(PIN_SERVO_LOWER);
    servoLower.writeMicroseconds(tokens[1]);
  }

  if (tokens[2] == 0) {
    servoLever.detach();
  } else {
    servoLever.attach(PIN_SERVO_LEVER);
    servoLever.writeMicroseconds(tokens[2]);
  }
}

void loop() 
{
  if (wifiShouldRestart)
    handleWifiRestart();

  if (scheduleRestart && scheduleRestart < millis())
    ESP.reset();
    
  server.handleClient();
}
